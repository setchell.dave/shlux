__test_simple () {
	rm -rf 000
	echo '[1,2,3]' | $lux
	shrun.sh dir2j 000 | json_pp
	echo
}

__test_more () {
	rm -rf 000
	$lux <<EOF
{
	"this": "has",
	an_idea: [ 1, 2, ["aaa", "bbb"], 3 ],
	"then": "we"
}
EOF
	shrun.sh dir2j 000 | json_pp
	echo
}
