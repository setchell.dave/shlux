#!/bin/sh
: ${SH_BIN:=@bindir@}
: ${SH_SHARE:=@shlibdir@}
test "$SH_BIN" != ${SH_BIN#@} && SH_BIN=$PWD
test $SH_SHARE != ${SH_SHARE#@} && SH_SHARE=./shlib
. $SH_SHARE/.func.sh
. $SH_SHARE/.msg.sh

while getopts h arg
do
    case "$arg" in
    h) _gen_help; exit 0 ;;
    esac
done; unset arg; shift $(( $OPTIND - 1 )); OPTIND=1
NONCE=Q_$(_r)_Q

_with state lex
_cmd_state lex update pattern <<'EOF'
string:"[^"]*"
literal:\(true\|false\|null\)
structural:[][{}\,:]
number:[0-9]\+
string:[^[:space:]:"]\+
null:.*
EOF

echo 1 | _cmd_state lex update index 

_jstring () {
	s_reps=$(printf 's/\\\"/%s/g' "$NONCE")
	tr '\n' ' ' | sed "$s_reps"
}

_junstring () {
	un_reps=$(printf 's/%s/\"/g' "$NONCE")
	sed 's/^[[:space:]]*//' | sed "$un_reps"
}

_get_tok () {
	test $(_cmd_state lex read index) -le $(_cmd_state lex lines lexed) \
		|| return 1
	lexline=$(_cmd_state lex read lexed | sed -n "$(_cmd_state lex read index)p")
	CUR_TOKEN="${lexline%%:*}"
	CUR_VALUE=$(echo "${lexline#*:}" | base64 -d)
}

_bump_idx () {
	echo $(( $(_cmd_state lex read index) + 1 )) | _cmd_state lex update index
}

_create_token () { # parseName -> token -> Parser
	echo $2 | _cmd_state $1 update token
}

_call_token () { # parseName -> Result
	(_get_tok || return 1
	test "$(_cmd_state $1 read token)" = "$CUR_TOKEN" || return 1
	r=$(_result)
	echo "$CUR_VALUE" | _cmd_state $r update value
	echo $1 | _cmd_state $r update parser
	_bump_idx; echo $r
	)
}

_create_keyword () { # parseName -> token -> value -> Parser
	echo $2 | _cmd_state $1 update token
	echo $3 | _cmd_state $1 update value
}

_call_keyword () { # parseName -> Result
	(_get_tok || return 1
	test "$(_cmd_state $1 read token)" = "$CUR_TOKEN" -a \
		"$(_cmd_state $1 read value)" = "$CUR_VALUE" || return 1
	r=$(_result)
	echo "$CUR_VALUE" | _cmd_state $r update value
	echo $1 | _cmd_state $r update parser
	_bump_idx; echo $r
	)
}

_create_opt () { # parseName -> optParse -> Parser
	echo $2 | _cmd_state $1 update optional
}

_call_opt () { # parseName -> Result
	(pId=$(_cmd_state $1 read optional)
	r=$(_result); tmpfile=$(mktemp -p $tmp)
	$(_cmd_state $pId read call) $pId >$tmpfile || return 0
	<$tmpfile _cmd_state $r update result
	echo $1 | _cmd_state $r update parser
	echo $r)
}

_create_rep () { # parseName -> repParse -> Bool
	echo $2 | _cmd_state $1 update repeat
}

_call_rep () { # parseName -> [Result]
	(pId=$(_cmd_state $1 read repeat); retval=1
	r=$(_result); tmpfile=$(mktemp -p $tmp)
	while $(_cmd_state $pId read call) $pId >>$tmpfile
	do retval=0; done
	# more correct to `return 1` here and wrap a repeat in an optional
	test $retval -eq 0 || return 0
	<$tmpfile _cmd_state $r update result
	echo $1 | _cmd_state $r update parser
	echo $r
	)
}

_create_concat () { # parseName -> parseLeft -> parseRight -> Parse
	pid=$1; shift
	printf '%s\n' "$@" | _cmd_state $pid update concat
}

_call_concat () { # parseName -> (Result, Result)
	(r=$(_result); tmpfile=$(mktemp -p $tmp)
	for p in $(_cmd_state $1 read concat)
	do
		$(_cmd_state $p read call) $p >>$tmpfile || return 1
	done
	<$tmpfile _cmd_state $r update result
	echo $1 | _cmd_state $r update parser
	echo $r)
}
		
_create_alternate () { # parseName -> parseLeft -> parseRight -> Parse
	pid=$1; shift
	printf '%s\n' "$@" | _cmd_state $pid update alts
}

_call_alternate () { # parseName -> Result
	(for p in $(_cmd_state $1 read alts)
	do
		$(_cmd_state $p read call) $p && return 0
	done
	return 1
	)
}

_result () {
	(r=$(_r 10 hex)
	_with state $r
	echo $r)
}

_create_parser () { # parseName -> create func -> call func -> exec func -> ParserObject
	_with state $1
	echo $2 | _cmd_state $1 update create
	echo $3 | _cmd_state $1 update call
	echo $4 | _cmd_state $1 update exec
}

_get_input () {
	_jstring | _cmd_state lex update input
}

_lex () {
	while _cmd_state lex extant input
	do
		for dd in $(_cmd_state lex read pattern)
		do
			tok_m=""; tok_64=""
			pattern="^[[:space:]]*${dd#*:}"; tag="${dd%%:*}"
			tok_m=$(_cmd_state lex read input | tee $tmp/lex.state \
				| grep -o "${pattern}")
			if test -n "$tok_m"
			then
				tok_64=$(echo "$tok_m" | sed 's/^[[:space:]]*"//; s/"$//' \
					| _junstring | base64 | tr '\n' ' ' | tr -d ' ')
				test "$tag" != null && { printf '%s:%s\n' $tag "$tok_64" \
					| _cmd_state lex append lexed ;}
				<$tmp/lex.state cut -c$(( 1 + $(expr length "$tok_m") ))- \
					| sed '$ { /^[[:space:]]*$/ d }' | _cmd_state lex update input
				break
			fi
		done
	done
}

_exec_json () { # result -> exec
	(ri=000
	for r in $(_cmd_state $1 read result)
	do
		if _cmd_state $r extant result
		then
			mkdir -p $ri
			f=''; f=$(_cmd_state $(_cmd_state $r read parser) read exec)
			test -z "$f" && return 1
			(cd $ri; $f $r)
		else
			_cmd_state $r read value >$ri
		fi
		ri=$(printf '%0.3d' $(( $ri + 1 )))
	done)
}

_exec_list () {
	(ri=000; touch .list
	lv=$(_cmd_state $1 read result | sed -n 2p)
	for r in $(_cmd_state $lv read result)
	do
		rv=$(_cmd_state $r read result | sed -n 1p)
		if _cmd_state $rv extant result
		then
			mkdir -p $ri
			f=''; f=$(_cmd_state $(_cmd_state $rv read parser) read exec)
			test -z "$f" && return 1
			(cd $ri; $f $rv)
		else
			_cmd_state $rv read value >$ri
		fi
		ri=$(printf '%0.3d' $(( $ri + 1 )))
	done
	)
}

_exec_dict () {
	(dv=$(_cmd_state $1 read result | sed -n 2p)
	for r in $(_cmd_state $dv read result)
	do
		key=$(_cmd_state $(_cmd_state $r read result | sed -n 1p) read value)
		mv=$(_cmd_state $r read result | sed -n 3p)
		rv=$(_cmd_state $mv read result | sed -n 1p)
		if _cmd_state $rv extant result
		then
			mkdir -p $key
			f=''; f=$(_cmd_state $(_cmd_state $rv read parser) read exec)
			test -z "$f" && return 1
			(cd $key; $f $rv)
		else
			_cmd_state $rv read value >$key
		fi
	done
	)
}

_create_parser json _create_rep _call_rep _exec_json
$(_cmd_state json read create) json jobj
_create_parser jobj _create_alternate _call_alternate
$(_cmd_state jobj read create) jobj dict list value

_create_parser listV _create_rep _call_rep
$(_cmd_state listV read create) listV multijValue
_create_parser list _create_concat _call_concat _exec_list
$(_cmd_state list read create) list lstart listV lstop
_create_parser lstart _create_keyword _call_keyword 
$(_cmd_state lstart read create) lstart structural "["
_create_parser lstop _create_keyword _call_keyword 
$(_cmd_state lstop read create) lstop structural "]"

_create_parser comma _create_keyword _call_keyword 
$(_cmd_state comma read create) comma structural ','
_create_parser commaOpt _create_opt _call_opt 
$(_cmd_state commaOpt read create) commaOpt comma

_create_parser dictV _create_rep _call_rep 
$(_cmd_state dictV read create) dictV dobj
_create_parser dict _create_concat _call_concat _exec_dict
$(_cmd_state dict read create) dict dstart dictV dstop
_create_parser dstart _create_keyword _call_keyword 
$(_cmd_state dstart read create) dstart structural '{'
_create_parser dstop _create_keyword _call_keyword 
$(_cmd_state dstop read create) dstop structural '}'

_create_parser colon _create_keyword _call_keyword 
$(_cmd_state colon read create) colon structural ':' 

_create_parser dobj _create_concat _call_concat 
$(_cmd_state dobj read create) dobj string colon multijValue

_create_parser multijValue _create_concat _call_concat 
$(_cmd_state multijValue read create) multijValue jobj commaOpt

_create_parser value _create_alternate _call_alternate 
$(_cmd_state value read create) value string number literal

_create_parser string _create_token _call_token 
$(_cmd_state string read create) string string

_create_parser number _create_token _call_token
$(_cmd_state number read create) number number

_create_parser literal _create_token _call_token 
$(_cmd_state literal read create) literal literal


DONE=true
: ${cmd:=${1:-parse}}; test -n "$1" && shift
case $cmd in
*) DONE=false ;;
esac
$DONE && return 0

case $cmd in
parse)
	_get_input
	_lex
	$(_cmd_state json read call) json >foo
	_exec_json $(cat foo) '.'
	;;
*) _${cmd} "$@" ;; # say anything
esac

